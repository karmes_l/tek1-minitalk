/*
** client.c for  in /home/karmes_l/Projets/Systeme_Unix/minitalk/v1/minitalk/client
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar 16 14:01:06 2015 lionel karmes
** Last update Fri Mar 20 13:20:47 2015 lionel karmes
*/

#include "my.h"

int	requet_accepted = 0;

void	get_sigusr(int sign)
{
  if (signal(SIGUSR1, get_sigusr) == SIG_ERR)
    my_putstrerror("[ERROR] SIGUSR1\n");
  if (signal(SIGUSR2, get_sigusr) == SIG_ERR)
    my_putstrerror("[ERROR] SIGUSR2\n");
  if (sign == SIGUSR2)
    requet_accepted = 1;
  else
    my_putstr("Non\n");
}

int	server_occuped(int pid)
{
  int	time;

  time = 0;
  if (signal(SIGUSR1, get_sigusr) == SIG_ERR)
    my_putstrerror("[ERROR] SIGUSR1\n");
  if (signal(SIGUSR2, get_sigusr) == SIG_ERR)
    my_putstrerror("[ERROR] SIGUSR2\n");
  while (requet_accepted == 0)
    {
      kill(pid, SIGUSR1);
      time++;
      if (time == 6)
	return (0);
      if (usleep(1000000) != 0)
	usleep(1000000);
    }
  return (1);
}

int	client(int pid, char *str)
{
  int	i;
  int	bit;
  int	len;

  i = 0;
  if (server_occuped(pid) == 0)
    return (0);
  len = my_strlen(str);
  while (i <= len)
    {
      bit = 7;
      while (bit >= 0)
	{
	  if (kill(pid,
		   (((str[i] >> bit) & 1) == 0) ? SIGUSR1 : SIGUSR2) == -1)
	    {
	      my_putstrerror("[ERROR] : server injoignable\n");
	      return (-1);
	    }
	  usleep(400);
	  bit--;
	}
      i++;
    }
  return (1);
}

int	main(int ac, char **av)
{
  int	pid;

  if (ac > 2)
    {
      pid = my_getnbr(av[1]);
      if (pid < 1)
	{
	  my_putstrerror("[ERROR] le pid doit être > 0\n");
	  return (0);
	}
      if (client(pid, av[2]) == 0)
	my_putstrerror("[ERROR] Timed out\n");
    }
  else
    my_putstrerror("[ERROR] : Usage ./client pid str\n");
  return (0);
}
