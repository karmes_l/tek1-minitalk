/*
** main.c for  in /home/karmes_l/Projets/Systeme_Unix/minitalk/v1/minitalk
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar 16 13:45:02 2015 lionel karmes
** Last update Fri Mar 20 13:28:30 2015 lionel karmes
*/

#include "my.h"

pid_t		g_pid_request = 0;
int		g_time = 0;

int		process_not_occuped(pid_t pid_client)
{
  if (pid_client == g_pid_request || pid_client == getpid())
    {
      g_time = 0;
      g_pid_request = pid_client;
      return (1);
    }
  if (g_pid_request == 0)
    {
      g_time = 0;
      kill(pid_client, SIGUSR2);
      g_pid_request = pid_client;
      return (2);
    }
  return (0);
}

void		request_accepted(int sign)
{
  static int	i = 8;
  static char	str = 0;

  i--;
  if (g_pid_request == 0)
    {
      str = 0;
      i = 8;
    }
  if (sign == SIGUSR1)
    str = str | (0 << i);
  else if (sign == SIGUSR2)
    str = str | (1 << i);
  if (i == 0)
    {
      if (str == '\0')
	{
	  my_putchar('\n');
	  g_pid_request = 0;
	}
      else
	my_putchar(str);
      i = 8;
      str = 0;
    }
}

void		get_sigusr(int sign, siginfo_t *info, void *context)
{
  struct sigaction	action;
  int			server_not_occuped;

  (void) context;
  action.sa_sigaction = get_sigusr;
  action.sa_flags = SA_NODEFER | SA_SIGINFO;
  if (sigaction(SIGUSR1, &action, NULL) == -1)
    my_putstrerror("[ERROR] SIGUSR1\n");
  if (sigaction(SIGUSR2, &action, NULL) == -1)
    my_putstrerror("[ERROR] SIGUSR1\n");
  server_not_occuped = process_not_occuped(info->si_pid);
  if (server_not_occuped == 1)
    request_accepted(sign);
  else if (server_not_occuped == 0)
    kill(info->si_pid, SIGUSR1);
}

void			server()
{
  struct sigaction	action;

  my_putnbr(getpid());
  my_putchar('\n');
  action.sa_sigaction = get_sigusr;
  action.sa_flags = SA_NODEFER | SA_SIGINFO;
  while (1)
    {
      if (sigaction(SIGUSR1, &action, NULL) == -1)
	my_putstrerror("[ERROR] SIGUSR1\n");
      if (sigaction(SIGUSR2, &action, NULL) == -1)
	my_putstrerror("[ERROR] SIGUSR1\n");
      usleep(100);
      g_time++;
      if (g_time == 30000)
	{
	  g_time = 0;
	  g_pid_request = 0;
	  request_accepted(0);
	}
    }
}

int	main(int ac, char **av)
{
  (void) av;
  if (ac > 1)
    my_putstrerror("Not argument required.");
  else
    server();
  my_putchar('\n');
  return (0);
}
