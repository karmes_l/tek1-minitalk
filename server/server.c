/*
** main.c for  in /home/karmes_l/Projets/Systeme_Unix/minitalk/v1/minitalk
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar 16 13:45:02 2015 lionel karmes
** Last update Fri Mar 20 13:28:02 2015 lionel karmes
*/

#include "my.h"

int		g_time = 0;

void		get_sigusr_next()
{
  if (signal(SIGUSR1, get_sigusr) == SIG_ERR)
    my_putstrerror("[ERROR] SIGUSR1\n");
  if (signal(SIGUSR2, get_sigusr) == SIG_ERR)
    my_putstrerror("[ERROR] SIGUSR2\n");
}

void		get_sigusr(int sign)
{
  static int	i = 8;
  static char	str = 0;

  i--;
  g_time = 0;
  if (sign == 0)
    {
      i = 8;
      str = 0;
    }
  get_sigusr_next();
  if (sign == SIGUSR1)
    str = str | (0 << i);
  else if (sign == SIGUSR2)
    str = str | (1 << i);
  if (i == 0)
    {
      if (str == '\0')
	my_putchar('\n');
      else
	my_putchar(str);
      i = 8;
      str = 0;
    }
}

void	server()
{
  my_putnbr(getpid());
  my_putchar('\n');
  while (1)
    {
      get_sigusr_next();
      usleep(100);
      g_time++;
      if (g_time == 30000)
	{
	  get_sigusr(0);
	}
    }
}

int	main(int ac, char **av)
{
  (void) av;
  if (ac > 1)
    my_putstrerror("Not argument required.");
  else
    server();
  my_putchar('\n');
  return (0);
}
