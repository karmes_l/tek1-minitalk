
##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Mon Mar 16 15:08:55 2015 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	= -L./lib/ -lmy

NAMESERVER	= server/server

NAMECLIENT	= client/client

LIB	= lib/

LIBNAME	= $(addprefix $(LIB), libmy.a)

SRCSSERVER	= server/server.c

SRCSCLIENT	= client/client.c

SRCS2	= count_num.c \
	  convert_base.c \
	  my_charisalpha.c \
	  my_charisnum.c \
	  my_getnbr.c \
	  my_putchar.c \
	  my_putcharerror.c \
	  my_putnbr.c \
	  my_putstr.c \
	  my_putstrerror.c \
	  my_revstr.c \
	  my_show_wordtab.c \
	  my_strcapitalize.c \
	  my_strcat.c \
	  my_strcmp.c \
	  my_strcpy.c \
	  my_strdup.c \
	  my_str_isalpha.c \
	  my_str_islower.c \
	  my_str_isnum.c \
	  my_str_isprintable.c \
	  my_str_isupper.c \
	  my_str_to_wordtab.c \
	  my_strlen.c \
	  my_strlowcase.c \
	  my_strncat.c \
	  my_strncmp.c \
	  my_strncpy.c \
	  my_strnum_to_wordtab.c \
	  my_strstr.c \
	  my_strupcase.c \
	  my_swap.c \
	  pmalloc.c \
	  pow_10.c \
	  power.c \
	  get_next_line.c

SRCSLIB	= $(addprefix $(LIB), $(SRCS2))

OBJSSERVER	= $(SRCSSERVER:.c=.o)

OBJSCLIENT	= $(SRCSCLIENT:.c=.o)

OBJSLIB	= $(SRCSLIB:.c=.o)


all: $(LIBNAME) $(NAMESERVER) $(NAMECLIENT)

$(NAMESERVER): $(OBJSSERVER)
	$(CC) $(OBJSSERVER) -o $(NAMESERVER) $(LDFLAGS)

$(NAMECLIENT): $(OBJSCLIENT)
	$(CC) $(OBJSCLIENT) -o $(NAMECLIENT)  $(LDFLAGS)

$(LIBNAME): $(OBJSLIB)
	make -C $(LIB)
clean:
	$(RM) $(OBJSSERVER)
	$(RM) $(OBJSCLIENT)
	make clean -C $(LIB)

fclean: clean
	$(RM) $(NAMECLIENT)
	$(RM) $(NAMESERVER)
	make fclean -C $(LIB)

re: fclean all

.PHONY: all clean fclean re
