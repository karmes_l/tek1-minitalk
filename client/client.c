/*
** client.c for  in /home/karmes_l/Projets/Systeme_Unix/minitalk/v1/minitalk/client
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar 16 14:01:06 2015 lionel karmes
** Last update Fri Mar 20 13:18:12 2015 lionel karmes
*/

#include "my.h"

int	client(int pid, char *str)
{
  int	i;
  int	bit;
  int	len;

  i = 0;
  len = my_strlen(str);
  while (i <= len)
    {
      bit = 7;
      while (bit >= 0)
	{
	  if (kill(pid,
		   (((str[i] >> bit) & 1) == 0) ? SIGUSR1 : SIGUSR2) == -1)
	    {
	      my_putstrerror("[ERROR] : server injoingnable\n");
	      return (0);
	    }
	  usleep(400);
	  bit--;
	}
      i++;
    }
  return (1);
}

int	main(int ac, char **av)
{
  int	pid;

  if (ac > 2)
    {
      pid = my_getnbr(av[1]);
      if (pid < 1)
	{
	  my_putstrerror("[ERROR] le pid doit être > 0\n");
	  return (0);
	}
      client(pid, av[2]);
    }
  else
    my_putstrerror("[ERROR] : Usage ./client pid str\n");
  return (0);
}
