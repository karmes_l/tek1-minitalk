/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Fri Mar 20 13:04:08 2015 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include <sys/types.h>
# include <signal.h>
# include <stdlib.h>
# include <unistd.h>

# define MAX(v1, v2)	((v1) > (v2)) ? (v1) : (v2)

char		*convert_base(char *, char *, char *);
int		count_num(long nb);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putcharerror(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
void		my_putstr0(char *);
void		my_putstrerror(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*my_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_strncpy2(char *, char *, int, int);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
char		*my_strdup(char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
int		my_putchar2(int);
char		**my_str_to_wordtab(char *, char);
void		*pmalloc(int);
char		*get_next_line(int);
int		usleep(unsigned int useconds);
int		kill(pid_t pid, int sig);
void		get_sigusr(int);

#endif /* !MY_H_ */
