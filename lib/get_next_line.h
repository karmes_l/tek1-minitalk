/*
** get_next_line.h for  in /home/karmes_l/Projets/Prog_Elem/get_next_line
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov 15 14:28:14 2014 lionel karmes
** Last update Sun Feb 22 16:18:15 2015 lionel karmes
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

#include <unistd.h>

#define SIZE_TO_READ (10)

typedef	struct	s_line
{
  int		c;
  int		len;
  char		*str_line;
}		t_line;

char	*get_next_line(const int fd);
void		*pmalloc(int);

#endif /* !GET_NEXT_LINE_ */
